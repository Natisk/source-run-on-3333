class HomeController < ApplicationController

  def index
    @my_xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n <test_task>\n <param_1>some string</param_1>\n <param_2>2002-09-24-06:00</param_2>\n <some_code>some code code code</some_code>\n <some_price_param>1234</some_price_param>\n </test_task>\n"
    render xml: @my_xml
  end

end